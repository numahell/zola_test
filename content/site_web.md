+++
title = "Votre site web"
+++

# Créer votre site Internet

Ça y est, vous avez décidé de rendre visible votre activité sur Internet ! Pour cela, vous avez choisi de créer un site web, vitrine de vos réalisation et vos références.

Je vous propose la réalisation de votre site web, un accompagnement personnalisé et des conseils pour choisir les outils et l'hébergeur, ainsi qu'une documentation pour vous aider à maintenir votre site. Pour votre site internet, vous pouvez choisir entre deux formules.

Et pour avoir l'esprit tranquille, vous pouvez ensuite opter pour un support à l'année, pour des modifications mineures ou bien si votre site web a un problème.

# Les offres

### Page web vitrine (une page)

La formule plus simple : en une page, ce que vous faites, et vos coordonnées pour vous contacter. Cette page web repose sur une charte graphique épurée, orientée lisibilité. Votre logo, les couleurs et polices sont personnalisables.

**Prix : 1400€ HT**

### Site web vitrine (jusqu'à 5 pages)

La plupart du temps, vous n'avez pas besoin d'écrire des tartines ou de mettre en ligne des dizaines de pages pour mettre en valeur votre activité. Un site web c'est, le plus souvent : la page d'accueil, une page décrivant ce que vous proposez, une autre listant quelques réalisations, et une page de contact. La charte graphique par défaut est épurée, votre logo, les couleurs et polices sont personnalisables.

**Prix : 3900€ HT**

Les fonctionnalités incluses :

- intégration des contenus (jusqu'à 5 pages)
- une galerie photos
- un formulaire de contact
- des pages d'actualités

En option, sur devis :

- une charte graphique personnalisée
- la rédaction des contenus

Technologie (si ça vous parle) : générateur de site statique Zola, ou bien CMS Wordpress

### Site web sur-mesure

Vous avez besoin de plus de pages pour décrire votre activité, par exemple présenter les personnes avec lesquelles vous travaillez, vos valeurs, vos produits ?

Avant la réalisation, nous travaillons ensemble à décrire votre besoin, puis nous procédons par itérations, en faisant des points réguliers afin de valider ensemble si ce qui est fait répond toujours à votre besoin.

**Prix : sur devis.**

Technologie (si ça vous parle) : CMS Wordpress ou drupal

## Ce qui n'est pas inclus dans les offres

Mais sur lequel je peux vous conseiller

- nom de domaine (15 à 25€ / an)
- hébergement de votre site (70 à 100€ / an)
- analyses statistiques du nombre de visites (en option via Matomo)

## Comment nous travaillons ensemble

Je peux m'adapter, mais voici comment je procède :

- nous récoltons ensemble de ce qui va permettre de construire votre site : les contenus, des photos, un logo, des exemples de sites qui vous plaisent, votre charte graphique si vous en avez une
- je vous conseille pour choisir un hébergeur et vous explique comment enregistrer votre nom de domaine
- je travaille à l'adaptation aux couleurs et polices choisies
- selon la formule choisie, je travaille à la création des pages et l'intégration des textes et des images
- je vous transmets une première version du site
- nous nous rencontrons pour ajuster ensemble le contenu, et je vous accompagne pour vous rendre autonome sur la modification de votre site
- je finalise le site et procède à sa mise en ligne du site après votre validation