+++
title = "Ateliers"
+++

# Ateliers en lien avec le numérique

Le numérique envahit tous les espaces de nos vies. Mon souhait est de permettre à toutes et tous de se réapproprier ces espaces en aidant à mieux comprendre et maîtriser le numérique.

Plusieurs ateliers possibles :

- les enjeux sociaux et environnementaux du numérique
- des outils libres et éthique pour vos usages
- démystifier le numérique
