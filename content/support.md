+++
title = "Accompagnement"
+++

# Accompagnement à l'édition de votre site web

Vous avez déjà un site web, mais vous avez des difficultés à le terminer ou le mettre à jour. Je peux vous aider à le finaliser et vous rendre autonome pour le mettre à jour par la suite.

Une première session d'accompagnement peut parfois suffire.

* Nous faisons une première réunion pour visiter l'ensemble de votre site, là où vous avez besoin d'aide, avoir les accès et lister les actions à réaliser pour commencer
* Je réalise les actions que nous avons convenu
* Nous faisons une deuxième réunion pour voir le résultat, et réfléchir ensemble à d'autres améliorations
* Je vous fournis une documentation pour vous aider pour la suite

**Première session d'accompagnement et améliorations : 400€ HT**

Si besoin, je peux vous proposer de réaliser d'autres améliorations sur votre site

**Une session d'accompagnement / améliorations suivantes : 250€ HT**

CMS : Wordpress, Drupal, Spip. Autres : à étudier.

# Support annuel

Vous avez besoin de libérer du temps pour votre activité, mais aussi de faire vivre votre site internet pour ajouter vos nouvelles réalisations, ou mettre à jour vos coordonnées. Je peux vous proposer de le mettre à jour sur demande.

Le support annuel inclus :

* une rencontre pour visiter ensemble votre site internet et avoir les accès
* 3 demandes de mises à jour des pages existantes (photos, textes)
* réponse dans les 3 jours

**Support annuel : 650€ HT**
